from django.db import models


class Article(models.Model):
    image = models.CharField(max_length=200)
    heading = models.CharField(max_length=100)
    description = models.CharField(max_length=300)
    textfield = models.TextField(max_length=800)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{}, {}".format(self.heading, self.created)
